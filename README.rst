==============
varnish-objvar
==============

Compatibility
-------------

This is varnish-objvar for varnish-cache master.

Overview
--------

This bundle contains four varnish modules (vmods) implementing a next
level variable interface using the VCL object model:

* vmod_taskvar:

  `task` scoped variables as objects: Each client or backend request
  (`task`) has their own view of taskvar variables.

* vmod_topvar:

  `top` scoped variables as objects: Each client request including all
  esi include levels (`top`) has its own view of topvar variables.

* vmod_globalvar:

  globally (per vcl) scoped variables as objects: All vcl subroutines
  have the same view on variables, any change comes in effect
  immediately.

* vmod_constant:

  gobally (per vcl) scoped constants as objects: All vcl subroutines
  have the same view on constants.

Full type support
-----------------

The vmods in this bundle aim to support all native VCL types, which
currently are:

* ``ACL``

* ``BACKEND``

* ``BLOB``

* ``BODY``

* ``BOOL``

* ``BYTES``

* ``DURATION``

* ``HEADER``

* ``INT``

* ``IP``

* ``PROBE``

* ``REAL``

* ``STEVEDORE``

* ``STRANDS`` implicit via ``STRING``\ -folding

* ``STRING_LIST`` implicit via ``STRING``\ -folding

* ``STRING``

* ``TIME``


SYNOPSIS
========

Variable object construction is identical for all vmods from this
bundle, shown here by example of the ``int`` type, but the other types
mentioned above are supported likewise:

::

   import constant [from "path"] ;
   import globalvar [from "path"] ;
   import taskvar [from "path"] ;
   import topvar [from "path"] ;

   vcl_init {
	new xconst_int = constant.int(5);
	new xglob_int = globalvar.int(5);
	new xtask_int = taskvar.int(5);
	new xtop_int = topvar.int(5);
   }

All vmods support methods to ``.get()`` the value and check if the
variable is ``.defined()``.

All vmods except ``vmod_constant`` support ``.set()`` and
``.undefine()`` to assign a value to a variable or unset it.

``vmod_taskvar`` and ``vmod_topvar`` also support ``.protect()`` to
make a variable read-only for the rest of its lifetime and
``.protected()`` to check for protection status.

For details see the documentation of the individual vmods.

REQUIREMENTS
============

The VMOD requires the Varnish since version 6.0.0 or the master
branch. See the project repository for versions that are compatible
with other versions of Varnish.

INSTALLATION
============

See `INSTALL.rst <INSTALL.rst>`_ in the project source repository.


COPYRIGHT
=========

::

  Copyright 2018 UPLEX Nils Goroll Systemoptimierung
  All rights reserved

  This document is licensed under the same conditions as the varnish-objvar
  project. See LICENSE for details.

  Author: Nils Goroll <nils.goroll@uplex.de>
