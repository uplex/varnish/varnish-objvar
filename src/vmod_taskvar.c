/*-
 * Copyright 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <string.h>
#include <stdlib.h>

#include <cache/cache.h>
#include <vcl.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <vsa.h>

#include "vcc_taskvar_if.h"
#include "type_magics.h"
#include "type_cp.h"
#include "compat.h"
#include "tasktop.h"

#define task_common(ctx, t, v)				\
	do {						\
		if (ctx->method & VCL_MET_TASK_H)	\
			return (v);			\
							\
		t = VRT_priv_task(ctx, v);		\
							\
		if (t == NULL) {			\
			VRT_fail(ctx, "no priv_task");	\
			return (NULL);			\
		}					\
							\
		if (t->priv)				\
			return (t->priv);		\
	} while(0)

static const void *
state_r(VRT_CTX, void *v)
{
	struct vmod_priv *t;

	task_common(ctx, t, v);

	return (v);
}

static void *
state_l(VRT_CTX, void *v, size_t sz)
{
	struct vmod_priv *t;

	task_common(ctx, t, v);

	t->priv = WS_Alloc(ctx->ws, sz);
	if (t->priv == NULL) {
		VRT_fail(ctx, "out of workspace");
		return (NULL);
	}
	memcpy(t->priv, v, sz);

	return (t->priv);
}

#define tv_magic	0x1000

#define VMOD_TASKVAR_ACL_MAGIC		(tv_magic | OBJVAR_ACL_MAGIC)
#define VMOD_TASKVAR_BACKEND_MAGIC	(tv_magic | OBJVAR_BACKEND_MAGIC)
#define VMOD_TASKVAR_BLOB_MAGIC	(tv_magic | OBJVAR_BLOB_MAGIC)
#define VMOD_TASKVAR_BODY_MAGIC	(tv_magic | OBJVAR_BODY_MAGIC)
#define VMOD_TASKVAR_BOOL_MAGIC	(tv_magic | OBJVAR_BOOL_MAGIC)
#define VMOD_TASKVAR_BYTES_MAGIC	(tv_magic | OBJVAR_BYTES_MAGIC)
#define VMOD_TASKVAR_DURATION_MAGIC	(tv_magic | OBJVAR_DURATION_MAGIC)
#define VMOD_TASKVAR_HEADER_MAGIC	(tv_magic | OBJVAR_HEADER_MAGIC)
#define VMOD_TASKVAR_INT_MAGIC		(tv_magic | OBJVAR_INT_MAGIC)
#define VMOD_TASKVAR_IP_MAGIC		(tv_magic | OBJVAR_IP_MAGIC)
#define VMOD_TASKVAR_PROBE_MAGIC	(tv_magic | OBJVAR_PROBE_MAGIC)
#define VMOD_TASKVAR_REAL_MAGIC	(tv_magic | OBJVAR_REAL_MAGIC)
#define VMOD_TASKVAR_STEVEDORE_MAGIC	(tv_magic | OBJVAR_STEVEDORE_MAGIC)
#define VMOD_TASKVAR_STRING_MAGIC	(tv_magic | OBJVAR_STRING_MAGIC)
#define VMOD_TASKVAR_TIME_MAGIC	(tv_magic | OBJVAR_TIME_MAGIC)

#define VCC_TYPE(TYPE, type) var_code(vmod_, VMOD_TASKVAR_, vmod_taskvar_, TYPE, type)
#include "tbl_types.h"

VCL_VOID
vmod_string_foreach(VRT_CTX, struct VPFX(taskvar_string) *va,
    VCL_STRING s, VCL_SUB sub, VCL_STRING delim)
{
	char *p, *str, *save;
	struct vmod_taskvar_string *v =
	    state_l(ctx, va, sizeof *v);

	if (v == NULL || s == NULL)
		return;

	CHECK_OBJ(v, VMOD_TASKVAR_STRING_MAGIC);

	if (v->protected) {
		VRT_fail(ctx,
		    "attempt to set protected variable");
		return;
	}

	str = WS_Copy(ctx->ws, s, -1);
	if (str == NULL) {
		VRT_fail(ctx, ".foreach() out of workspace");
		return;
	}
	v->defined = 1;
	p = strtok_r(str, delim, &save);
	while (p != NULL) {
		v->var = p;
		VRT_call(ctx, sub);
		if (VRT_handled(ctx))
			break;
		p = strtok_r(NULL, delim, &save);
	}
	v->var = NULL;
	v->defined = 0;
}
