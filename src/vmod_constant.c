/*-
 * Copyright 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <string.h>
#include <stdlib.h>
#include <pthread.h>

#include <cache/cache.h>

#include <sys/socket.h>
#include <vsa.h>

#include "vcc_constant_if.h"
#include "type_magics.h"
#include "type_cp.h"
#include "compat.h"

#define tv_magic	0x4000

#define VMOD_CONSTANT_ACL_MAGIC		(tv_magic | OBJVAR_ACL_MAGIC)
#define VMOD_CONSTANT_BACKEND_MAGIC	(tv_magic | OBJVAR_BACKEND_MAGIC)
#define VMOD_CONSTANT_BLOB_MAGIC	(tv_magic | OBJVAR_BLOB_MAGIC)
#define VMOD_CONSTANT_BODY_MAGIC	(tv_magic | OBJVAR_BODY_MAGIC)
#define VMOD_CONSTANT_BOOL_MAGIC	(tv_magic | OBJVAR_BOOL_MAGIC)
#define VMOD_CONSTANT_BYTES_MAGIC	(tv_magic | OBJVAR_BYTES_MAGIC)
#define VMOD_CONSTANT_DURATION_MAGIC	(tv_magic | OBJVAR_DURATION_MAGIC)
#define VMOD_CONSTANT_HEADER_MAGIC	(tv_magic | OBJVAR_HEADER_MAGIC)
#define VMOD_CONSTANT_INT_MAGIC		(tv_magic | OBJVAR_INT_MAGIC)
#define VMOD_CONSTANT_IP_MAGIC		(tv_magic | OBJVAR_IP_MAGIC)
#define VMOD_CONSTANT_PROBE_MAGIC	(tv_magic | OBJVAR_PROBE_MAGIC)
#define VMOD_CONSTANT_REAL_MAGIC	(tv_magic | OBJVAR_REAL_MAGIC)
#define VMOD_CONSTANT_STEVEDORE_MAGIC	(tv_magic | OBJVAR_STEVEDORE_MAGIC)
#define VMOD_CONSTANT_STRING_MAGIC	(tv_magic | OBJVAR_STRING_MAGIC)
#define VMOD_CONSTANT_TIME_MAGIC	(tv_magic | OBJVAR_TIME_MAGIC)

#define var_code(vmod_, VMODPFX_, vmodpfx_, TYPE, type)			\
	struct vmodpfx_ ## type {					\
		uint16_t	magic;					\
		unsigned	defined:1;				\
		VCL_ ## TYPE	var;					\
	};								\
									\
	VCL_VOID							\
	vmod_ ## type ## __init(VRT_CTX,				\
	    struct vmodpfx_ ## type **vp,				\
	    const char *vcl_name,					\
	    struct VARGS(type ##__init) *args)				\
	{								\
		struct vmodpfx_ ## type *v;				\
									\
		AN(vp);							\
		AZ(*vp);						\
									\
		ALLOC_OBJ(v, VMODPFX_ ## TYPE ## _MAGIC);		\
		if (v == NULL) {					\
			VRT_fail(ctx, "%s: alloc failed", vcl_name);	\
			return;						\
		}							\
									\
		if (args->valid_init) {					\
			CP_ ## TYPE (v->var, args->init);		\
			v->defined = 1;					\
		}							\
		*vp = v;						\
	}								\
									\
	VCL_VOID							\
	vmod_ ## type ## __fini(struct vmodpfx_ ## type **vp)		\
	{								\
		struct vmodpfx_ ## type *v = *vp;			\
									\
		*vp = NULL;						\
		if (v == NULL)						\
			return;						\
									\
		CHECK_OBJ(v, VMODPFX_ ## TYPE ## _MAGIC);		\
		FREE_ ## TYPE (v->var);					\
		FREE_OBJ(v);						\
	}								\
									\
	VCL_ ## TYPE							\
	vmod_ ## type ## _get(VRT_CTX, struct vmodpfx_ ## type *v,	\
	    struct VARGS(type ## _get) *a)				\
	{								\
									\
		(void) ctx;						\
									\
		CHECK_OBJ_NOTNULL(v, VMODPFX_ ## TYPE ## _MAGIC);	\
									\
		if (v->defined)						\
			return (v->var);				\
									\
		if (a->valid_fallback)					\
			return (a->fallback);				\
		return (DEF_ ## TYPE(ctx));				\
	}								\
									\
	VCL_BOOL							\
	vmod_ ## type ## _defined(VRT_CTX, struct vmodpfx_ ## type *v) \
	{								\
									\
		(void) ctx;						\
									\
		CHECK_OBJ_NOTNULL(v, VMODPFX_ ## TYPE ## _MAGIC);	\
									\
		return (!!v->defined);					\
	}

#define VCC_TYPE(TYPE, type) var_code(vmod_, VMOD_CONSTANT_, vmod_constant_, TYPE, type)
#include "tbl_types.h"
