/*-
 * Copyright 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

static const char empty[1] = { '\0' };

static const struct vrt_blob null_blob[1] = {{
#define VMOD_OBJVAR_NULL_TYPE 0xfade40b1
		.type = VMOD_OBJVAR_NULL_TYPE,
		.len = 0,
		.blob = empty,
}};

/* based upon REPLACE() from miniobj.h */
#define cp_string(dst, src)						\
	do {								\
		char *d;						\
		if ((dst) != NULL)					\
			free(TRUST_ME(dst));				\
		if ((src) != NULL) {					\
			d = strdup(src);				\
			AN(d);						\
			(dst) = d;					\
		} else {						\
			(dst) = NULL;					\
		}							\
	} while(0)

#define free_string(x)				\
	if (x) {				\
		free(TRUST_ME(x));		\
		(x) = NULL;			\
	}					\
	(void)0

#define KIND_ACL		immediate
#define CP_ACL(dst, src)	(dst) = (src)
#define FREE_ACL(ptr)		(void)0

#define KIND_BACKEND		immediate
#define CP_BACKEND(dst, src)	VRT_Assign_Backend(&dst, src)
#define FREE_BACKEND(ptr)	VRT_Assign_Backend(&ptr, NULL)

#define KIND_BLOB		pointer
#define CP_BLOB(dst, src)						\
	do {								\
		unsigned char *spc;					\
		struct vrt_blob *d = TRUST_ME(dst);			\
									\
		FREE_BLOB(d);						\
		if ((src) == NULL || (src)->len == 0 ||			\
		    (src)->blob == NULL) {				\
			(dst) = null_blob;				\
		} else {						\
			spc = malloc(sizeof *d + PRNDUP((src)->len));	\
			AN(spc);					\
			d = (void *)spc;				\
			spc += sizeof *d;				\
			memcpy(spc, (src)->blob, (src)->len);		\
			d->blob = spc;					\
			d->len = (src)->len;				\
			(dst) = d;					\
		}							\
	} while(0)
#define FREE_BLOB(ptr)					\
	if ((ptr) != NULL && (ptr) != null_blob) {	\
		free(TRUST_ME(ptr));			\
		(ptr) = NULL;				\
	}						\
	(void)0

#define KIND_BODY		pointer
#define CP_BODY(dst, src)	cp_string(dst, src)
#define FREE_BODY(ptr)		free_string(ptr)

#define KIND_BOOL		immediate
#define CP_BOOL(dst, src)	(dst) = (src)
#define FREE_BOOL(ptr)		(void)0

#define KIND_BYTES		immediate
#define CP_BYTES(dst, src)	(dst) = (src)
#define FREE_BYTES(ptr)		(void)0

#define KIND_DURATION		immediate
#define CP_DURATION(dst, src)	(dst) = (src)
#define FREE_DURATION(ptr)	(void)0

#define KIND_HEADER		pointer
#define CP_HEADER(dst, src)						\
	do {								\
		const struct gethdr_s *o = dst;				\
		struct gethdr_s *h;					\
		FREE_HEADER(o);					\
		AN(src);						\
		AN((src)->what);					\
		h = malloc(sizeof *h);					\
		AN(h);							\
		h->where = (src)->where;				\
		h->what = strdup((src)->what);				\
		AN(h->what);						\
		(dst) = h;						\
	} while(0)
#define FREE_HEADER(ptr)				\
	if ((ptr) != NULL) {				\
		if ((ptr)->what)			\
			free(TRUST_ME((ptr)->what));	\
		free(TRUST_ME(ptr));			\
		(ptr) = NULL;				\
	}						\
	(void)0

#define KIND_INT		immediate
#define CP_INT(dst, src)	(dst) = (src)
#define FREE_INT(ptr)		(void)0

#define KIND_IP		pointer
#define CP_IP(dst, src)					\
	do {							\
		struct suckaddr *d;				\
		FREE_IP(dst);					\
		d = malloc(vsa_suckaddr_len);			\
		AN(d);						\
		memcpy(d, (src), vsa_suckaddr_len);		\
		(dst) = d;					\
	} while(0)
#define FREE_IP(ptr)				\
	if ((ptr) != NULL) {			\
		free(TRUST_ME(ptr));		\
		(ptr) = NULL;			\
	}					\
	(void)0

#define KIND_PROBE		immediate
#define CP_PROBE(dst, src)	(dst) = (src)
#define FREE_PROBE(ptr)		(void)0

#define KIND_REAL		immediate
#define CP_REAL(dst, src)	(dst) = (src)
#define FREE_REAL(ptr)		(void)0

#define KIND_STEVEDORE		immediate
#define CP_STEVEDORE(dst, src)	(dst) = (src)
#define FREE_STEVEDORE(ptr)	(void)0

#define KIND_STRING		pointer
#define CP_STRING(dst, src)	cp_string(dst, src)
#define FREE_STRING(ptr)	free_string(ptr)

#define KIND_TIME		immediate
#define CP_TIME(dst, src)	(dst) = (src)
#define FREE_TIME(ptr)		(void)0
