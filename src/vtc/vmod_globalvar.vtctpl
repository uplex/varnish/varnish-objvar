varnishtest "test vmod-globalvar"

server s1 {
#define VCC_TYPE(TYPE, type)					\
	rxreq\n						\
	txresp
#include "../tbl_vtc.h"
} -start

varnish v1 \\
	-arg "-sstv1=default,1m" \\
	-arg "-sstv2=default,1m" \\
	-vcl+backend {

    import globalvar;
    import blob;

    acl acl1 { "127.0.0.1"/32; }
    acl acl2 { "127.0.0.2"/32; }

    probe p1 { .url = "/1"; }
    probe p2 { .url = "/2"; }

    backend b1 { .host = "127.0.0.1"; }
    backend b2 { .host = "127.0.0.2"; }

    sub vcl_init {
	new blob1 = blob.blob(encoded="1");
	new blob2 = blob.blob(encoded="2");

	#define VCC_TYPE(TYPE, type)				\
	new x ## type = globalvar. %% type ();			\
	new xd ## type = globalvar. %% type ( VTC1_ ## TYPE );
	#include "../tbl_vtc.h"
    }

    sub vcl_recv {
	#define VCC_TYPE(TYPE, type)				\
	if (req.http.what == #type) {				\
	    set req.http.def1-x  = x ## type %% .defined();	\
	    set req.http.def1-xd = xd ## type %% .defined();	\
	    set req.http.get1-x = CMP_ ## TYPE (		\
	      x ## type %% .get( VTC2_ ## TYPE ),		\
	      VTC2_ ## TYPE );					\
	    set req.http.get1-xd = CMP_ ## TYPE (		\
	      xd ## type %% .get( VTC2_ ## TYPE ),		\
	      VTC2_ ## TYPE );					\
	    x ## type %% .set( VTC2_ ## TYPE );			\
	    xd ## type %% .set( VTC2_ ## TYPE );		\
	} else
	#include "../tbl_vtc.h"
	{ return (fail); }
	return (pass);
    }


    sub vcl_deliver {
	set resp.http.def1-x = req.http.def1-x;
	set resp.http.def1-xd = req.http.def1-xd;
	set resp.http.get1-x = req.http.get1-x;
	set resp.http.get1-xd = req.http.get1-xd;

	#define VCC_TYPE(TYPE, type)				\
	if (req.http.what == #type) {				\
	    set resp.http.def2-x  = x ## type %% .defined();	\
	    set resp.http.def2-xd = xd ## type %% .defined();	\
	    set resp.http.cmp2 = CMP_ ## TYPE (			\
	      x ## type %% .get( VTC1_ ## TYPE ),		\
	      xd ## type %% .get( VTC1_ ## TYPE ));		\
	} else
	#include "../tbl_vtc.h"
	{ return (fail); }
    }

    sub vcl_backend_fetch {
	#define VCC_TYPE(TYPE, type)				\
	if (bereq.http.what == #type) {				\
	    set bereq.http.bedef1-x  = x ## type %% .defined();	\
	    set bereq.http.bedef1-xd = xd ## type %% .defined();\
	    set bereq.http.beget1-x = CMP_ ## TYPE (		\
	      x ## type %% .get( VTC2_ ## TYPE ),		\
	      VTC2_ ## TYPE );					\
	    set bereq.http.beget1-xd = CMP_ ## TYPE (		\
	      xd ## type %% .get( VTC2_ ## TYPE ),		\
	      VTC2_ ## TYPE );					\
	    x ## type %% .set( VTC2_ ## TYPE );			\
	    xd ## type %% .set( VTC2_ ## TYPE );		\
	} else
	#include "../tbl_vtc.h"
	{ return (fail); }
    }

    sub vcl_backend_response {
	set beresp.http.bedef1-x = bereq.http.bedef1-x;
	set beresp.http.bedef1-xd = bereq.http.bedef1-xd;
	set beresp.http.beget1-x = bereq.http.beget1-x;
	set beresp.http.beget1-xd = bereq.http.beget1-xd;

	#define VCC_TYPE(TYPE, type)				\
	if (bereq.http.what == #type) {				\
	    set beresp.http.bedef2-x  = x ## type %% .defined();\
	    set beresp.http.bedef2-xd = xd ## type %% .defined();\
	    set beresp.http.becmp2 = CMP_ ## TYPE (		\
	      x ## type %% .get( VTC1_ ## TYPE ),		\
	      xd ## type %% .get( VTC1_ ## TYPE ));		\
	} else
	#include "../tbl_vtc.h"
	{ return (fail); }
    }
} -start


#define VCC_TYPE(TYPE, type)					\
client c ## type {\n						\
	txreq -hdr {what: type}\n				\
	rxresp\n						\
	expect resp.status == 200\n				\
	expect resp.http.def1-x == "false"\n			\
	expect resp.http.def1-xd == "true"\n			\
	expect resp.http.get1-x == "true"\n			\
	expect resp.http.get1-xd == "false"\n			\
	expect resp.http.def2-x == "true"\n			\
	expect resp.http.def2-xd == "true"\n			\
	expect resp.http.cmp2 == "true"\n			\
	expect resp.http.bedef1-x == "true"\n			\
	expect resp.http.bedef1-xd == "true"\n			\
	expect resp.http.beget1-x == "true"\n			\
	expect resp.http.beget1-xd == "true"\n			\
	expect resp.http.bedef2-x == "true"\n			\
	expect resp.http.bedef2-xd == "true"\n			\
	expect resp.http.becmp2 == "true"\n			\
} -run
#include "../tbl_vtc.h"
