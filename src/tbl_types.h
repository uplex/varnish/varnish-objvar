/* subset of tbl/vcc_types.h which we support */

/* standard text for defaults doc */
#define DEFDOC The default `fallback` is
#define DEFFAIL(ctx, typ) VRT_fail(ctx, #typ "undefined and no fallback provided")
#define DEFERR Using .get() on an undefined value without a fallback triggers a VCL error.

// NULL support needs https://github.com/varnishcache/varnish-cache/pull/3622
#define DEF_ACL(ctx) DEFFAIL(ctx, acl), NULL
#define DEFDOC_ACL DEFERR
VCC_TYPE(ACL, acl)

#define DEF_BACKEND(ctx) NULL
#define DEFDOC_BACKEND DEFDOC the ``None`` backend.
VCC_TYPE(BACKEND, backend)

#define DEF_BLOB(ctx) vrt_null_blob
#define DEFDOC_BLOB DEFDOC the null blob.
VCC_TYPE(BLOB, blob)

// NULL OK? There is a lack of vmod examples taking NULL as an argument
#define DEF_BODY(ctx) DEFFAIL(ctx, body), NULL
#define DEFDOC_BODY DEFERR
VCC_TYPE(BODY, body)

#define DEF_BOOL(ctx) 0
#define DEFDOC_BOOL DEFDOC false
VCC_TYPE(BOOL, bool)

#define DEF_BYTES(ctx) 0
#define DEFDOC_BYTES DEFDOC 0 bytes.
VCC_TYPE(BYTES, bytes)

#define DEF_DURATION(ctx) 0
#define DEFDOC_DURATION DEFDOC 0s.
VCC_TYPE(DURATION, duration)

// VCC_TYPE(ENUM, enum)

#define DEF_HEADER(ctx) DEFFAIL(ctx, header), NULL
#define DEFDOC_HEADER DEFERR
VCC_TYPE(HEADER, header)

// VCC_TYPE(HTTP, http)
// VCC_TYPE(INSTANCE, instance)

#define DEF_INT(ctx) 0
#define DEFDOC_INT DEFDOC 0.
VCC_TYPE(INT, int)

#define DEF_IP(ctx) bogo_ip
#define DEFDOC_IP DEFDOC IPv4 0.0.0.0.
VCC_TYPE(IP, ip)

#define DEF_PROBE(ctx) NULL
#define DEFDOC_PROBE DEFDOC no backend.
VCC_TYPE(PROBE, probe)

#define DEF_REAL(ctx) 0.0
#define DEFDOC_REAL DEFDOC 0.0.
VCC_TYPE(REAL, real)

#define DEF_STEVEDORE(ctx) NULL
#define DEFDOC_STEVEDORE DEFDOC the default storage.
VCC_TYPE(STEVEDORE, stevedore)

// VCC_TYPE(STRANDS)

#define DEF_STRING(ctx) ""
#define DEFDOC_STRING DEFDOC the empty string.
VCC_TYPE(STRING, string)
#ifdef VCCTPL
$Method VOID .foreach(STRING string, SUB sub, STRING delim=",")

Breaks ``string`` into a sequence of zero or more nonempty tokens,
seperated by ``delim``, and successively calls ``sub`` with the object
set to each token.

Processing ends for a nonempty ``return()`` from ``sub``.
#endif

// VCC_TYPE(STRINGS)
// VCC_TYPE(STRING_LIST)
// VCC_TYPE(SUB)

#define DEF_TIME(ctx) 0
#define DEFDOC_TIME DEFDOC the epoch (1970/1/1 0:00:00 GMT).
VCC_TYPE(TIME, time)

// VCC_TYPE(VCL)
// VCC_TYPE(VOID)
#undef VCC_TYPE
