#!/bin/sh

if [ "x${VARNISHSRC}" = "x" ] ; then
	echo >&2 VARNISHSRC needs to point to varnish-cache sources
	exit 9
fi

# need to look at vmods individually, otherwise we get error 18
# all over the place
for f in constant globalvar taskvar topvar ; do
    flexelint \
	-D__FLEXELINT__ \
	${VARNISHSRC}/flint.lnt \
	flint.lnt \
	-zero \
	-I.. \
	-I${VARNISHSRC}/include \
	-I${VARNISHSRC}/bin/varnishd \
	vcc_${f}_if.c vmod_${f}.c
    2>&1
done
