/* subset of tbl/vcc_types.h which we can test */

#define VTC1_ACL acl1
#define VTC2_ACL acl2
#define CMP_ACL(a, b) a == b
VCC_TYPE(ACL, acl)

#define VTC1_BACKEND b1
#define VTC2_BACKEND b2
#define CMP_BACKEND(a, b) a == b
VCC_TYPE(BACKEND, backend)

#define VTC1_BLOB blob1.get()
#define VTC2_BLOB blob2.get()
#define CMP_BLOB(a, b) blob.equal(a, b)
VCC_TYPE(BLOB, blob)

// body support is very rudimentary
//#define VTC1_BODY
//#define VTC2_BODY
//VCC_TYPE(BODY, body)

// #2809 VCC bug for xx = bool1 == bool2
//#define VTC1_BOOL true
//#define VTC2_BOOL false
//#define CMP_BOOL(a, b) a == b
//VCC_TYPE(BOOL, bool)

#define VTC1_BYTES 1MB
#define VTC2_BYTES 2GB
#define CMP_BYTES(a, b) a == b
VCC_TYPE(BYTES, bytes)

#define VTC1_DURATION 1s
#define VTC2_DURATION 2h
#define CMP_DURATION(a, b) a == b
VCC_TYPE(DURATION, duration)

// VCC_TYPE(ENUM, enum)

// no way to get a header in vcl_init{}
//#define VTC1_HEADER
//#define VTC2_HEADER
//#define CMP_HEADER(a, b) a == b
//VCC_TYPE(HEADER, header)

// VCC_TYPE(HTTP, http)
// VCC_TYPE(INSTANCE, instance)

#define VTC1_INT 1
#define VTC2_INT 2
#define CMP_INT(a, b) a == b
VCC_TYPE(INT, int)

#define VTC1_IP "127.0.0.1"
#define VTC2_IP "127.0.0.2"
#define CMP_IP(a, b) a == b
VCC_TYPE(IP, ip)

#define VTC1_PROBE p1
#define VTC2_PROBE p2
#define CMP_PROBE(a, b) a == b
VCC_TYPE(PROBE, probe)

#define VTC1_REAL 1.1
#define VTC2_REAL 2.2
#define CMP_REAL(a, b) a == b
VCC_TYPE(REAL, real)

#define VTC1_STEVEDORE storage.stv1
#define VTC2_STEVEDORE storage.stv2
#define CMP_STEVEDORE(a, b) a == b
VCC_TYPE(STEVEDORE, stevedore)

// VCC_TYPE(STRANDS)

#define VTC1_STRING "1"
#define VTC2_STRING "2"
#define CMP_STRING(a, b) a == b
VCC_TYPE(STRING, string)

// VCC_TYPE(STRINGS)
// VCC_TYPE(STRING_LIST)
// VCC_TYPE(SUB)

#define VTC1_TIME now + 1h
#define VTC2_TIME now + 2h
#define CMP_TIME(a, b) (b - a < 10m)
VCC_TYPE(TIME, time)

// VCC_TYPE(VCL)
// VCC_TYPE(VOID)
#undef VCC_TYPE
