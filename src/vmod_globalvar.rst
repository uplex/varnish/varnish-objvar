..
.. NB:  This file is machine generated, DO NOT EDIT!
..
.. Edit ./vmod_globalvar.vcc and run make instead
..


:tocdepth: 1

.. _vmod_globalvar(3):

====================================================
VMOD globalvar - global variables as objects for VCL
====================================================

SYNOPSIS
========

.. parsed-literal::

  import globalvar [as name] [from "path"]
  
  :ref:`globalvar.acl()`
  
      :ref:`xacl.get()`
  
      :ref:`xacl.set()`
  
      :ref:`xacl.undefine()`
  
      :ref:`xacl.defined()`
  
  :ref:`globalvar.backend()`
  
      :ref:`xbackend.get()`
  
      :ref:`xbackend.set()`
  
      :ref:`xbackend.undefine()`
  
      :ref:`xbackend.defined()`
  
  :ref:`globalvar.blob()`
  
      :ref:`xblob.get()`
  
      :ref:`xblob.set()`
  
      :ref:`xblob.undefine()`
  
      :ref:`xblob.defined()`
  
  :ref:`globalvar.body()`
  
      :ref:`xbody.get()`
  
      :ref:`xbody.set()`
  
      :ref:`xbody.undefine()`
  
      :ref:`xbody.defined()`
  
  :ref:`globalvar.bool()`
  
      :ref:`xbool.get()`
  
      :ref:`xbool.set()`
  
      :ref:`xbool.undefine()`
  
      :ref:`xbool.defined()`
  
  :ref:`globalvar.bytes()`
  
      :ref:`xbytes.get()`
  
      :ref:`xbytes.set()`
  
      :ref:`xbytes.undefine()`
  
      :ref:`xbytes.defined()`
  
  :ref:`globalvar.duration()`
  
      :ref:`xduration.get()`
  
      :ref:`xduration.set()`
  
      :ref:`xduration.undefine()`
  
      :ref:`xduration.defined()`
  
  :ref:`globalvar.header()`
  
      :ref:`xheader.get()`
  
      :ref:`xheader.set()`
  
      :ref:`xheader.undefine()`
  
      :ref:`xheader.defined()`
  
  :ref:`globalvar.int()`
  
      :ref:`xint.get()`
  
      :ref:`xint.set()`
  
      :ref:`xint.undefine()`
  
      :ref:`xint.defined()`
  
  :ref:`globalvar.ip()`
  
      :ref:`xip.get()`
  
      :ref:`xip.set()`
  
      :ref:`xip.undefine()`
  
      :ref:`xip.defined()`
  
  :ref:`globalvar.probe()`
  
      :ref:`xprobe.get()`
  
      :ref:`xprobe.set()`
  
      :ref:`xprobe.undefine()`
  
      :ref:`xprobe.defined()`
  
  :ref:`globalvar.real()`
  
      :ref:`xreal.get()`
  
      :ref:`xreal.set()`
  
      :ref:`xreal.undefine()`
  
      :ref:`xreal.defined()`
  
  :ref:`globalvar.stevedore()`
  
      :ref:`xstevedore.get()`
  
      :ref:`xstevedore.set()`
  
      :ref:`xstevedore.undefine()`
  
      :ref:`xstevedore.defined()`
  
  :ref:`globalvar.string()`
  
      :ref:`xstring.get()`
  
      :ref:`xstring.set()`
  
      :ref:`xstring.undefine()`
  
      :ref:`xstring.defined()`
  
  :ref:`globalvar.time()`
  
      :ref:`xtime.get()`
  
      :ref:`xtime.set()`
  
      :ref:`xtime.undefine()`
  
      :ref:`xtime.defined()`
  

DESCRIPTION
===========

This module implements globally (per vcl) scoped variables as objects:
All vcl subroutines have the same view on variables, any change comes
in effect immediately.

The value of a globalvar is the either the initial value from the
constructor or the last value assigned by any task executing the
respective vcl.

If no value is set, it is undefined. A globalvar can also be undefined
using the ``.undefine()`` method.

Attempts to ``.get()`` an undefined value will return the ``.get()``
method`s `fallback` argument. For most types, there is a default
fallback; if there is none, using ``.get()`` on an undefined value
triggers a VCL failure at runtime.

Where necessary, read access to globalvar variables is reference
counted using priv_task state, such that changed variables` storage is
not freed before the last access ceased.

Example with an initially undefined variable::

 import globalvar;

 sub vcl_init {
  new xint = globalvar.int();
 }

 sub vcl_recv {
  set req.http.def = xint.defined(); # false
  set req.http.fallback = xint.get(5); # 5
  xint.set(42);
 }

 \# in some other request, later

 sub vcl_deliver {
  set resp.http.the-answer = xint.get(); # 42
 }

The implementation of the various classes of this vmod is
auto-generated, and so is the documentation. Thus, the documentation
following this generic description is identical for all types except
for the respective type names and the ``.get()`` fallback.

.. just a newline

.. _globalvar.acl():

new xacl = globalvar.acl([ACL init])
------------------------------------

Construct a(n) ACL globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xacl.get():

ACL xacl.get([ACL fallback])
----------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. Using .get() on an undefined value without a fallback triggers a VCL error.

.. _xacl.set():

VOID xacl.set(ACL)
------------------

Set the value of the globalvar.

.. _xacl.undefine():

VOID xacl.undefine()
--------------------

Undefine the globalvar.

.. _xacl.defined():

BOOL xacl.defined()
-------------------

Return whether the globalvar is defined.

.. _globalvar.backend():

new xbackend = globalvar.backend([BACKEND init])
------------------------------------------------

Construct a(n) BACKEND globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xbackend.get():

BACKEND xbackend.get([BACKEND fallback])
----------------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is the ``None`` backend.

.. _xbackend.set():

VOID xbackend.set(BACKEND)
--------------------------

Set the value of the globalvar.

.. _xbackend.undefine():

VOID xbackend.undefine()
------------------------

Undefine the globalvar.

.. _xbackend.defined():

BOOL xbackend.defined()
-----------------------

Return whether the globalvar is defined.

.. _globalvar.blob():

new xblob = globalvar.blob([BLOB init])
---------------------------------------

Construct a(n) BLOB globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xblob.get():

BLOB xblob.get([BLOB fallback])
-------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is the null blob.

.. _xblob.set():

VOID xblob.set(BLOB)
--------------------

Set the value of the globalvar.

.. _xblob.undefine():

VOID xblob.undefine()
---------------------

Undefine the globalvar.

.. _xblob.defined():

BOOL xblob.defined()
--------------------

Return whether the globalvar is defined.

.. _globalvar.body():

new xbody = globalvar.body([BODY init])
---------------------------------------

Construct a(n) BODY globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xbody.get():

BODY xbody.get([BODY fallback])
-------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. Using .get() on an undefined value without a fallback triggers a VCL error.

.. _xbody.set():

VOID xbody.set(BODY)
--------------------

Set the value of the globalvar.

.. _xbody.undefine():

VOID xbody.undefine()
---------------------

Undefine the globalvar.

.. _xbody.defined():

BOOL xbody.defined()
--------------------

Return whether the globalvar is defined.

.. _globalvar.bool():

new xbool = globalvar.bool([BOOL init])
---------------------------------------

Construct a(n) BOOL globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xbool.get():

BOOL xbool.get([BOOL fallback])
-------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is false

.. _xbool.set():

VOID xbool.set(BOOL)
--------------------

Set the value of the globalvar.

.. _xbool.undefine():

VOID xbool.undefine()
---------------------

Undefine the globalvar.

.. _xbool.defined():

BOOL xbool.defined()
--------------------

Return whether the globalvar is defined.

.. _globalvar.bytes():

new xbytes = globalvar.bytes([BYTES init])
------------------------------------------

Construct a(n) BYTES globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xbytes.get():

BYTES xbytes.get([BYTES fallback])
----------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is 0 bytes.

.. _xbytes.set():

VOID xbytes.set(BYTES)
----------------------

Set the value of the globalvar.

.. _xbytes.undefine():

VOID xbytes.undefine()
----------------------

Undefine the globalvar.

.. _xbytes.defined():

BOOL xbytes.defined()
---------------------

Return whether the globalvar is defined.

.. _globalvar.duration():

new xduration = globalvar.duration([DURATION init])
---------------------------------------------------

Construct a(n) DURATION globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xduration.get():

DURATION xduration.get([DURATION fallback])
-------------------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is 0s.

.. _xduration.set():

VOID xduration.set(DURATION)
----------------------------

Set the value of the globalvar.

.. _xduration.undefine():

VOID xduration.undefine()
-------------------------

Undefine the globalvar.

.. _xduration.defined():

BOOL xduration.defined()
------------------------

Return whether the globalvar is defined.

.. _globalvar.header():

new xheader = globalvar.header([HEADER init])
---------------------------------------------

Construct a(n) HEADER globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xheader.get():

HEADER xheader.get([HEADER fallback])
-------------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. Using .get() on an undefined value without a fallback triggers a VCL error.

.. _xheader.set():

VOID xheader.set(HEADER)
------------------------

Set the value of the globalvar.

.. _xheader.undefine():

VOID xheader.undefine()
-----------------------

Undefine the globalvar.

.. _xheader.defined():

BOOL xheader.defined()
----------------------

Return whether the globalvar is defined.

.. _globalvar.int():

new xint = globalvar.int([INT init])
------------------------------------

Construct a(n) INT globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xint.get():

INT xint.get([INT fallback])
----------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is 0.

.. _xint.set():

VOID xint.set(INT)
------------------

Set the value of the globalvar.

.. _xint.undefine():

VOID xint.undefine()
--------------------

Undefine the globalvar.

.. _xint.defined():

BOOL xint.defined()
-------------------

Return whether the globalvar is defined.

.. _globalvar.ip():

new xip = globalvar.ip([IP init])
---------------------------------

Construct a(n) IP globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xip.get():

IP xip.get([IP fallback])
-------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is IPv4 0.0.0.0.

.. _xip.set():

VOID xip.set(IP)
----------------

Set the value of the globalvar.

.. _xip.undefine():

VOID xip.undefine()
-------------------

Undefine the globalvar.

.. _xip.defined():

BOOL xip.defined()
------------------

Return whether the globalvar is defined.

.. _globalvar.probe():

new xprobe = globalvar.probe([PROBE init])
------------------------------------------

Construct a(n) PROBE globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xprobe.get():

PROBE xprobe.get([PROBE fallback])
----------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is no backend.

.. _xprobe.set():

VOID xprobe.set(PROBE)
----------------------

Set the value of the globalvar.

.. _xprobe.undefine():

VOID xprobe.undefine()
----------------------

Undefine the globalvar.

.. _xprobe.defined():

BOOL xprobe.defined()
---------------------

Return whether the globalvar is defined.

.. _globalvar.real():

new xreal = globalvar.real([REAL init])
---------------------------------------

Construct a(n) REAL globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xreal.get():

REAL xreal.get([REAL fallback])
-------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is 0.0.

.. _xreal.set():

VOID xreal.set(REAL)
--------------------

Set the value of the globalvar.

.. _xreal.undefine():

VOID xreal.undefine()
---------------------

Undefine the globalvar.

.. _xreal.defined():

BOOL xreal.defined()
--------------------

Return whether the globalvar is defined.

.. _globalvar.stevedore():

new xstevedore = globalvar.stevedore([STEVEDORE init])
------------------------------------------------------

Construct a(n) STEVEDORE globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xstevedore.get():

STEVEDORE xstevedore.get([STEVEDORE fallback])
----------------------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is the default storage.

.. _xstevedore.set():

VOID xstevedore.set(STEVEDORE)
------------------------------

Set the value of the globalvar.

.. _xstevedore.undefine():

VOID xstevedore.undefine()
--------------------------

Undefine the globalvar.

.. _xstevedore.defined():

BOOL xstevedore.defined()
-------------------------

Return whether the globalvar is defined.

.. _globalvar.string():

new xstring = globalvar.string([STRING init])
---------------------------------------------

Construct a(n) STRING globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xstring.get():

STRING xstring.get([STRING fallback])
-------------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is the empty string.

.. _xstring.set():

VOID xstring.set(STRING)
------------------------

Set the value of the globalvar.

.. _xstring.undefine():

VOID xstring.undefine()
-----------------------

Undefine the globalvar.

.. _xstring.defined():

BOOL xstring.defined()
----------------------

Return whether the globalvar is defined.

.. _globalvar.time():

new xtime = globalvar.time([TIME init])
---------------------------------------

Construct a(n) TIME globalvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xtime.get():

TIME xtime.get([TIME fallback])
-------------------------------

Return the value of the globalvar or the `fallback` argument
if it is undefined. The default `fallback` is the epoch (1970/1/1 0:00:00 GMT).

.. _xtime.set():

VOID xtime.set(TIME)
--------------------

Set the value of the globalvar.

.. _xtime.undefine():

VOID xtime.undefine()
---------------------

Undefine the globalvar.

.. _xtime.defined():

BOOL xtime.defined()
--------------------

Return whether the globalvar is defined.


SEE ALSO
========

vcl\(7),varnishd\(1)

COPYRIGHT
=========

::

  Copyright 2018-2022 UPLEX Nils Goroll Systemoptimierung
  
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:
  
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the
     distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
