..
.. NB:  This file is machine generated, DO NOT EDIT!
..
.. Edit ./vmod_topvar.vcc and run make instead
..


:tocdepth: 1

.. _vmod_topvar(3):

==========================================================
VMOD topvar - cross-ESI (top) variables as objects for VCL
==========================================================

SYNOPSIS
========

.. parsed-literal::

  import topvar [as name] [from "path"]
  
  :ref:`topvar.acl()`
  
      :ref:`xacl.get()`
  
      :ref:`xacl.set()`
  
      :ref:`xacl.undefine()`
  
      :ref:`xacl.defined()`
  
      :ref:`xacl.protect()`
  
      :ref:`xacl.protected()`
  
  :ref:`topvar.backend()`
  
      :ref:`xbackend.get()`
  
      :ref:`xbackend.set()`
  
      :ref:`xbackend.undefine()`
  
      :ref:`xbackend.defined()`
  
      :ref:`xbackend.protect()`
  
      :ref:`xbackend.protected()`
  
  :ref:`topvar.blob()`
  
      :ref:`xblob.get()`
  
      :ref:`xblob.set()`
  
      :ref:`xblob.undefine()`
  
      :ref:`xblob.defined()`
  
      :ref:`xblob.protect()`
  
      :ref:`xblob.protected()`
  
  :ref:`topvar.body()`
  
      :ref:`xbody.get()`
  
      :ref:`xbody.set()`
  
      :ref:`xbody.undefine()`
  
      :ref:`xbody.defined()`
  
      :ref:`xbody.protect()`
  
      :ref:`xbody.protected()`
  
  :ref:`topvar.bool()`
  
      :ref:`xbool.get()`
  
      :ref:`xbool.set()`
  
      :ref:`xbool.undefine()`
  
      :ref:`xbool.defined()`
  
      :ref:`xbool.protect()`
  
      :ref:`xbool.protected()`
  
  :ref:`topvar.bytes()`
  
      :ref:`xbytes.get()`
  
      :ref:`xbytes.set()`
  
      :ref:`xbytes.undefine()`
  
      :ref:`xbytes.defined()`
  
      :ref:`xbytes.protect()`
  
      :ref:`xbytes.protected()`
  
  :ref:`topvar.duration()`
  
      :ref:`xduration.get()`
  
      :ref:`xduration.set()`
  
      :ref:`xduration.undefine()`
  
      :ref:`xduration.defined()`
  
      :ref:`xduration.protect()`
  
      :ref:`xduration.protected()`
  
  :ref:`topvar.header()`
  
      :ref:`xheader.get()`
  
      :ref:`xheader.set()`
  
      :ref:`xheader.undefine()`
  
      :ref:`xheader.defined()`
  
      :ref:`xheader.protect()`
  
      :ref:`xheader.protected()`
  
  :ref:`topvar.int()`
  
      :ref:`xint.get()`
  
      :ref:`xint.set()`
  
      :ref:`xint.undefine()`
  
      :ref:`xint.defined()`
  
      :ref:`xint.protect()`
  
      :ref:`xint.protected()`
  
  :ref:`topvar.ip()`
  
      :ref:`xip.get()`
  
      :ref:`xip.set()`
  
      :ref:`xip.undefine()`
  
      :ref:`xip.defined()`
  
      :ref:`xip.protect()`
  
      :ref:`xip.protected()`
  
  :ref:`topvar.probe()`
  
      :ref:`xprobe.get()`
  
      :ref:`xprobe.set()`
  
      :ref:`xprobe.undefine()`
  
      :ref:`xprobe.defined()`
  
      :ref:`xprobe.protect()`
  
      :ref:`xprobe.protected()`
  
  :ref:`topvar.real()`
  
      :ref:`xreal.get()`
  
      :ref:`xreal.set()`
  
      :ref:`xreal.undefine()`
  
      :ref:`xreal.defined()`
  
      :ref:`xreal.protect()`
  
      :ref:`xreal.protected()`
  
  :ref:`topvar.stevedore()`
  
      :ref:`xstevedore.get()`
  
      :ref:`xstevedore.set()`
  
      :ref:`xstevedore.undefine()`
  
      :ref:`xstevedore.defined()`
  
      :ref:`xstevedore.protect()`
  
      :ref:`xstevedore.protected()`
  
  :ref:`topvar.string()`
  
      :ref:`xstring.get()`
  
      :ref:`xstring.set()`
  
      :ref:`xstring.undefine()`
  
      :ref:`xstring.defined()`
  
      :ref:`xstring.protect()`
  
      :ref:`xstring.protected()`
  
  :ref:`topvar.time()`
  
      :ref:`xtime.get()`
  
      :ref:`xtime.set()`
  
      :ref:`xtime.undefine()`
  
      :ref:`xtime.defined()`
  
      :ref:`xtime.protect()`
  
      :ref:`xtime.protected()`
  

DESCRIPTION
===========

This module implements `top` scoped variables as objects: Each client
request including all esi include levels (`top`) has its own view of
topvar variables.

A topvar can be constructed with a default, which is used as the
initial value at the beginning of each request at esi level 0.

The value of a topvar is the either the default or the last value
assigned to it within any esi subrequest. If neither a default nor
previous value was set, then the value is undefined.

A topvar can also be undefined using the ``.undefine()`` method.

Attempts to ``.get()`` an undefined value will return the ``.get()``
method`s `fallback` argument. For most types, there is a default
fallback; if there is none, using ``.get()`` on an undefined value
triggers a VCL failure at runtime.

topvar variables can be protected against write access, in which case
any attempt to ``.set()`` them triggers a VCL failure at runtime.

Example::

 import topvar;

 sub vcl_init {
  new xint = topvar.int();
 }

 sub vcl_recv {
  if (req.esi_level == 0) {
   set req.http.def = xint.defined(); # false
   set req.http.fallback = xint.get(5); # 5
   xint.set(42);
  } else {
   set req.http.fallback = xint.get(5); #42
  }
 }

 sub vcl_deliver {
  set resp.http.the-answer = xint.get(); # 42
 }

The implementation of the various classes of this vmod is
auto-generated, and so is the documentation. Thus, the documentation
following this generic description is identical for all types except
for the respective type names and the ``.get()`` fallback.

.. just a newline

.. _topvar.acl():

new xacl = topvar.acl([ACL init])
---------------------------------

Construct a(n) ACL topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xacl.get():

ACL xacl.get([ACL fallback])
----------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. Using .get() on an undefined value without a fallback triggers a VCL error.

Restricted to: ``client``.



.. _xacl.set():

VOID xacl.set(ACL)
------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xacl.undefine():

VOID xacl.undefine()
--------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xacl.defined():

BOOL xacl.defined()
-------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xacl.protect():

VOID xacl.protect()
-------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xacl.protected():

BOOL xacl.protected()
---------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.backend():

new xbackend = topvar.backend([BACKEND init])
---------------------------------------------

Construct a(n) BACKEND topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xbackend.get():

BACKEND xbackend.get([BACKEND fallback])
----------------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is the ``None`` backend.

Restricted to: ``client``.



.. _xbackend.set():

VOID xbackend.set(BACKEND)
--------------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xbackend.undefine():

VOID xbackend.undefine()
------------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xbackend.defined():

BOOL xbackend.defined()
-----------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xbackend.protect():

VOID xbackend.protect()
-----------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xbackend.protected():

BOOL xbackend.protected()
-------------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.blob():

new xblob = topvar.blob([BLOB init])
------------------------------------

Construct a(n) BLOB topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xblob.get():

BLOB xblob.get([BLOB fallback])
-------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is the null blob.

Restricted to: ``client``.



.. _xblob.set():

VOID xblob.set(BLOB)
--------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xblob.undefine():

VOID xblob.undefine()
---------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xblob.defined():

BOOL xblob.defined()
--------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xblob.protect():

VOID xblob.protect()
--------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xblob.protected():

BOOL xblob.protected()
----------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.body():

new xbody = topvar.body([BODY init])
------------------------------------

Construct a(n) BODY topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xbody.get():

BODY xbody.get([BODY fallback])
-------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. Using .get() on an undefined value without a fallback triggers a VCL error.

Restricted to: ``client``.



.. _xbody.set():

VOID xbody.set(BODY)
--------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xbody.undefine():

VOID xbody.undefine()
---------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xbody.defined():

BOOL xbody.defined()
--------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xbody.protect():

VOID xbody.protect()
--------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xbody.protected():

BOOL xbody.protected()
----------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.bool():

new xbool = topvar.bool([BOOL init])
------------------------------------

Construct a(n) BOOL topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xbool.get():

BOOL xbool.get([BOOL fallback])
-------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is false

Restricted to: ``client``.



.. _xbool.set():

VOID xbool.set(BOOL)
--------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xbool.undefine():

VOID xbool.undefine()
---------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xbool.defined():

BOOL xbool.defined()
--------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xbool.protect():

VOID xbool.protect()
--------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xbool.protected():

BOOL xbool.protected()
----------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.bytes():

new xbytes = topvar.bytes([BYTES init])
---------------------------------------

Construct a(n) BYTES topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xbytes.get():

BYTES xbytes.get([BYTES fallback])
----------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is 0 bytes.

Restricted to: ``client``.



.. _xbytes.set():

VOID xbytes.set(BYTES)
----------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xbytes.undefine():

VOID xbytes.undefine()
----------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xbytes.defined():

BOOL xbytes.defined()
---------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xbytes.protect():

VOID xbytes.protect()
---------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xbytes.protected():

BOOL xbytes.protected()
-----------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.duration():

new xduration = topvar.duration([DURATION init])
------------------------------------------------

Construct a(n) DURATION topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xduration.get():

DURATION xduration.get([DURATION fallback])
-------------------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is 0s.

Restricted to: ``client``.



.. _xduration.set():

VOID xduration.set(DURATION)
----------------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xduration.undefine():

VOID xduration.undefine()
-------------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xduration.defined():

BOOL xduration.defined()
------------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xduration.protect():

VOID xduration.protect()
------------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xduration.protected():

BOOL xduration.protected()
--------------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.header():

new xheader = topvar.header([HEADER init])
------------------------------------------

Construct a(n) HEADER topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xheader.get():

HEADER xheader.get([HEADER fallback])
-------------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. Using .get() on an undefined value without a fallback triggers a VCL error.

Restricted to: ``client``.



.. _xheader.set():

VOID xheader.set(HEADER)
------------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xheader.undefine():

VOID xheader.undefine()
-----------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xheader.defined():

BOOL xheader.defined()
----------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xheader.protect():

VOID xheader.protect()
----------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xheader.protected():

BOOL xheader.protected()
------------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.int():

new xint = topvar.int([INT init])
---------------------------------

Construct a(n) INT topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xint.get():

INT xint.get([INT fallback])
----------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is 0.

Restricted to: ``client``.



.. _xint.set():

VOID xint.set(INT)
------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xint.undefine():

VOID xint.undefine()
--------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xint.defined():

BOOL xint.defined()
-------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xint.protect():

VOID xint.protect()
-------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xint.protected():

BOOL xint.protected()
---------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.ip():

new xip = topvar.ip([IP init])
------------------------------

Construct a(n) IP topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xip.get():

IP xip.get([IP fallback])
-------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is IPv4 0.0.0.0.

Restricted to: ``client``.



.. _xip.set():

VOID xip.set(IP)
----------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xip.undefine():

VOID xip.undefine()
-------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xip.defined():

BOOL xip.defined()
------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xip.protect():

VOID xip.protect()
------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xip.protected():

BOOL xip.protected()
--------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.probe():

new xprobe = topvar.probe([PROBE init])
---------------------------------------

Construct a(n) PROBE topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xprobe.get():

PROBE xprobe.get([PROBE fallback])
----------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is no backend.

Restricted to: ``client``.



.. _xprobe.set():

VOID xprobe.set(PROBE)
----------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xprobe.undefine():

VOID xprobe.undefine()
----------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xprobe.defined():

BOOL xprobe.defined()
---------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xprobe.protect():

VOID xprobe.protect()
---------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xprobe.protected():

BOOL xprobe.protected()
-----------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.real():

new xreal = topvar.real([REAL init])
------------------------------------

Construct a(n) REAL topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xreal.get():

REAL xreal.get([REAL fallback])
-------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is 0.0.

Restricted to: ``client``.



.. _xreal.set():

VOID xreal.set(REAL)
--------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xreal.undefine():

VOID xreal.undefine()
---------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xreal.defined():

BOOL xreal.defined()
--------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xreal.protect():

VOID xreal.protect()
--------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xreal.protected():

BOOL xreal.protected()
----------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.stevedore():

new xstevedore = topvar.stevedore([STEVEDORE init])
---------------------------------------------------

Construct a(n) STEVEDORE topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xstevedore.get():

STEVEDORE xstevedore.get([STEVEDORE fallback])
----------------------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is the default storage.

Restricted to: ``client``.



.. _xstevedore.set():

VOID xstevedore.set(STEVEDORE)
------------------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xstevedore.undefine():

VOID xstevedore.undefine()
--------------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xstevedore.defined():

BOOL xstevedore.defined()
-------------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xstevedore.protect():

VOID xstevedore.protect()
-------------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xstevedore.protected():

BOOL xstevedore.protected()
---------------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.string():

new xstring = topvar.string([STRING init])
------------------------------------------

Construct a(n) STRING topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xstring.get():

STRING xstring.get([STRING fallback])
-------------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is the empty string.

Restricted to: ``client``.



.. _xstring.set():

VOID xstring.set(STRING)
------------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xstring.undefine():

VOID xstring.undefine()
-----------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xstring.defined():

BOOL xstring.defined()
----------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xstring.protect():

VOID xstring.protect()
----------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xstring.protected():

BOOL xstring.protected()
------------------------

Return whether the topvar is protected.

Restricted to: ``client``.



.. _topvar.time():

new xtime = topvar.time([TIME init])
------------------------------------

Construct a(n) TIME topvar with the default value `init`,
if provided. If no `init` value is provided, the variable
will be initially undefined.

.. _xtime.get():

TIME xtime.get([TIME fallback])
-------------------------------

Return the value of the topvar or the `fallback` argument
if it is undefined. The default `fallback` is the epoch (1970/1/1 0:00:00 GMT).

Restricted to: ``client``.



.. _xtime.set():

VOID xtime.set(TIME)
--------------------

Set the value of the topvar.

Triggers a vcl failure for protected variables.

Restricted to: ``client``.



.. _xtime.undefine():

VOID xtime.undefine()
---------------------

Undefine the topvar.

Restricted to: ``client``.



.. _xtime.defined():

BOOL xtime.defined()
--------------------

Return whether the topvar is defined.

Restricted to: ``client``.



.. _xtime.protect():

VOID xtime.protect()
--------------------

Protect the topvar, so any future .set() calls on it
trigger a vcl failure.

Restricted to: ``client``.



.. _xtime.protected():

BOOL xtime.protected()
----------------------

Return whether the topvar is protected.

Restricted to: ``client``.

SEE ALSO
========

vcl\(7),varnishd\(1)

COPYRIGHT
=========

::

  Copyright 2018-2022 UPLEX Nils Goroll Systemoptimierung
  
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:
  
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the
     distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
