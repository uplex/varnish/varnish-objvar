..
.. NB:  This file is machine generated, DO NOT EDIT!
..
.. Edit ./vmod_constant.vcc and run make instead
..


:tocdepth: 1

.. _vmod_constant(3):

=================================
VMOD constant - constants for VCL
=================================

SYNOPSIS
========

.. parsed-literal::

  import constant [as name] [from "path"]
  
  :ref:`constant.acl()`
  
      :ref:`xacl.get()`
  
      :ref:`xacl.defined()`
  
  :ref:`constant.backend()`
  
      :ref:`xbackend.get()`
  
      :ref:`xbackend.defined()`
  
  :ref:`constant.blob()`
  
      :ref:`xblob.get()`
  
      :ref:`xblob.defined()`
  
  :ref:`constant.body()`
  
      :ref:`xbody.get()`
  
      :ref:`xbody.defined()`
  
  :ref:`constant.bool()`
  
      :ref:`xbool.get()`
  
      :ref:`xbool.defined()`
  
  :ref:`constant.bytes()`
  
      :ref:`xbytes.get()`
  
      :ref:`xbytes.defined()`
  
  :ref:`constant.duration()`
  
      :ref:`xduration.get()`
  
      :ref:`xduration.defined()`
  
  :ref:`constant.header()`
  
      :ref:`xheader.get()`
  
      :ref:`xheader.defined()`
  
  :ref:`constant.int()`
  
      :ref:`xint.get()`
  
      :ref:`xint.defined()`
  
  :ref:`constant.ip()`
  
      :ref:`xip.get()`
  
      :ref:`xip.defined()`
  
  :ref:`constant.probe()`
  
      :ref:`xprobe.get()`
  
      :ref:`xprobe.defined()`
  
  :ref:`constant.real()`
  
      :ref:`xreal.get()`
  
      :ref:`xreal.defined()`
  
  :ref:`constant.stevedore()`
  
      :ref:`xstevedore.get()`
  
      :ref:`xstevedore.defined()`
  
  :ref:`constant.string()`
  
      :ref:`xstring.get()`
  
      :ref:`xstring.defined()`
  
  :ref:`constant.time()`
  
      :ref:`xtime.get()`
  
      :ref:`xtime.defined()`
  

DESCRIPTION
===========

This module implements globally (per vcl) scoped constants as objects:
All vcl subroutines have the same view on constants.

The value of a constant is the value assigned to it at construction
time, if any.

constants are undefined if no value is assigned in the constructor.

Attempts to ``.get()`` an undefined value will return the ``.get()``
method`s `fallback` argument. For most types, there is a default
fallback; if there is none, using ``.get()`` on an undefined value
triggers a VCL failure at runtime.

Example::

 import constant;

 sub vcl_init {
  new xint = constant.int(42);
  new xundef = constant.int();
 }

 sub vcl_recv {
  set req.http.def = xundef.defined(); # false
  set req.http.fallback = xundef.get(5); # 5
 }

 sub vcl_deliver {
  set resp.http.the-answer = xint.get(); # 42
 }

The implementation of the various classes of this vmod is
auto-generated, and so is the documentation. Thus, the documentation
following this generic description is identical for all types except
for the respective type names and the ``.get()`` fallback.

.. just a newline

.. _constant.acl():

new xacl = constant.acl([ACL init])
-----------------------------------

Construct a(n) ACL constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xacl.get():

ACL xacl.get([ACL fallback])
----------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. Using .get() on an undefined value without a fallback triggers a VCL error.

.. _xacl.defined():

BOOL xacl.defined()
-------------------

Return whether the constant is defined.

.. _constant.backend():

new xbackend = constant.backend([BACKEND init])
-----------------------------------------------

Construct a(n) BACKEND constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xbackend.get():

BACKEND xbackend.get([BACKEND fallback])
----------------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is the ``None`` backend.

.. _xbackend.defined():

BOOL xbackend.defined()
-----------------------

Return whether the constant is defined.

.. _constant.blob():

new xblob = constant.blob([BLOB init])
--------------------------------------

Construct a(n) BLOB constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xblob.get():

BLOB xblob.get([BLOB fallback])
-------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is the null blob.

.. _xblob.defined():

BOOL xblob.defined()
--------------------

Return whether the constant is defined.

.. _constant.body():

new xbody = constant.body([BODY init])
--------------------------------------

Construct a(n) BODY constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xbody.get():

BODY xbody.get([BODY fallback])
-------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. Using .get() on an undefined value without a fallback triggers a VCL error.

.. _xbody.defined():

BOOL xbody.defined()
--------------------

Return whether the constant is defined.

.. _constant.bool():

new xbool = constant.bool([BOOL init])
--------------------------------------

Construct a(n) BOOL constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xbool.get():

BOOL xbool.get([BOOL fallback])
-------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is false

.. _xbool.defined():

BOOL xbool.defined()
--------------------

Return whether the constant is defined.

.. _constant.bytes():

new xbytes = constant.bytes([BYTES init])
-----------------------------------------

Construct a(n) BYTES constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xbytes.get():

BYTES xbytes.get([BYTES fallback])
----------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is 0 bytes.

.. _xbytes.defined():

BOOL xbytes.defined()
---------------------

Return whether the constant is defined.

.. _constant.duration():

new xduration = constant.duration([DURATION init])
--------------------------------------------------

Construct a(n) DURATION constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xduration.get():

DURATION xduration.get([DURATION fallback])
-------------------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is 0s.

.. _xduration.defined():

BOOL xduration.defined()
------------------------

Return whether the constant is defined.

.. _constant.header():

new xheader = constant.header([HEADER init])
--------------------------------------------

Construct a(n) HEADER constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xheader.get():

HEADER xheader.get([HEADER fallback])
-------------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. Using .get() on an undefined value without a fallback triggers a VCL error.

.. _xheader.defined():

BOOL xheader.defined()
----------------------

Return whether the constant is defined.

.. _constant.int():

new xint = constant.int([INT init])
-----------------------------------

Construct a(n) INT constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xint.get():

INT xint.get([INT fallback])
----------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is 0.

.. _xint.defined():

BOOL xint.defined()
-------------------

Return whether the constant is defined.

.. _constant.ip():

new xip = constant.ip([IP init])
--------------------------------

Construct a(n) IP constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xip.get():

IP xip.get([IP fallback])
-------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is IPv4 0.0.0.0.

.. _xip.defined():

BOOL xip.defined()
------------------

Return whether the constant is defined.

.. _constant.probe():

new xprobe = constant.probe([PROBE init])
-----------------------------------------

Construct a(n) PROBE constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xprobe.get():

PROBE xprobe.get([PROBE fallback])
----------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is no backend.

.. _xprobe.defined():

BOOL xprobe.defined()
---------------------

Return whether the constant is defined.

.. _constant.real():

new xreal = constant.real([REAL init])
--------------------------------------

Construct a(n) REAL constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xreal.get():

REAL xreal.get([REAL fallback])
-------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is 0.0.

.. _xreal.defined():

BOOL xreal.defined()
--------------------

Return whether the constant is defined.

.. _constant.stevedore():

new xstevedore = constant.stevedore([STEVEDORE init])
-----------------------------------------------------

Construct a(n) STEVEDORE constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xstevedore.get():

STEVEDORE xstevedore.get([STEVEDORE fallback])
----------------------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is the default storage.

.. _xstevedore.defined():

BOOL xstevedore.defined()
-------------------------

Return whether the constant is defined.

.. _constant.string():

new xstring = constant.string([STRING init])
--------------------------------------------

Construct a(n) STRING constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xstring.get():

STRING xstring.get([STRING fallback])
-------------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is the empty string.

.. _xstring.defined():

BOOL xstring.defined()
----------------------

Return whether the constant is defined.

.. _constant.time():

new xtime = constant.time([TIME init])
--------------------------------------

Construct a(n) TIME constant with the value `init`,
if provided. If no `init` value is provided, the constant
will be undefined.

.. _xtime.get():

TIME xtime.get([TIME fallback])
-------------------------------

Return the value of the constant or the `fallback` argument
if it is undefined. The default `fallback` is the epoch (1970/1/1 0:00:00 GMT).

.. _xtime.defined():

BOOL xtime.defined()
--------------------

Return whether the constant is defined.

SEE ALSO
========

vcl\(7),varnishd\(1)

COPYRIGHT
=========

::

  Copyright 2018-2022 UPLEX Nils Goroll Systemoptimierung
  
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:
  
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the
     distribution.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
