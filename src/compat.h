/* varnish-cache 48b9d1bf538fe563f505b75043f297d91690caa3 */
#ifndef VRT_NULL_BLOB_TYPE
#define VRT_NULL_BLOB_TYPE 0xfade4fa0
static const struct vrt_blob *vrt_null_blob = &(struct vrt_blob){
	.type = VRT_NULL_BLOB_TYPE,
	.len = 0,
	.blob = "\0"
};
#endif
