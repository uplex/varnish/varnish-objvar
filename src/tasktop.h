/*-
 * Copyright 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#define var_code(vmod_, VMODPFX_, vmodpfx_, TYPE, type)			\
	struct vmodpfx_ ## type {					\
		uint16_t	magic;					\
		int		defined:1;				\
		int		protected:1;				\
		VCL_ ## TYPE	var;					\
	};								\
									\
	VCL_VOID							\
	vmod_ ## type ## __init(VRT_CTX,				\
	    struct vmodpfx_ ## type **vp,				\
	    const char *vcl_name,					\
	    struct VARGS(type ##__init) *args)				\
	{								\
		struct vmodpfx_ ## type *v;				\
									\
		AN(vp);							\
		AZ(*vp);						\
									\
		ALLOC_OBJ(v, VMODPFX_ ## TYPE ## _MAGIC);		\
		if (v == NULL) {					\
			VRT_fail(ctx, "%s: alloc failed", vcl_name);	\
			return;						\
		}							\
									\
		if (args->valid_init) {					\
			CP_ ## TYPE (v->var, args->init);		\
			v->defined = 1;					\
		}							\
		*vp = v;						\
	}								\
									\
	VCL_VOID							\
	vmod_ ## type ## __fini(struct vmodpfx_ ## type **vp)		\
	{								\
		struct vmodpfx_ ## type *v = *vp;			\
									\
		*vp = NULL;						\
		if (v == NULL)						\
			return;						\
									\
		CHECK_OBJ(v, VMODPFX_ ## TYPE ## _MAGIC);		\
		FREE_ ## TYPE (v->var);					\
		FREE_OBJ(v);						\
	}								\
									\
	VCL_ ## TYPE							\
	vmod_ ## type ## _get(VRT_CTX, struct vmodpfx_ ## type *va,	\
	    struct VARGS(type ## _get) *a)				\
	{								\
		const struct vmodpfx_ ## type *v = state_r(ctx, va);	\
									\
		if (v == NULL || ! v->defined) {			\
			if (a->valid_fallback)				\
				return (a->fallback);			\
			return (DEF_ ## TYPE(ctx));			\
		}							\
									\
		CHECK_OBJ(v, VMODPFX_ ## TYPE ## _MAGIC);		\
									\
		return (v->var);					\
	}								\
									\
	VCL_VOID							\
	vmod_ ## type ## _set(VRT_CTX, struct vmodpfx_ ## type *va,	\
	    VCL_ ## TYPE val)						\
	{								\
		struct vmodpfx_ ## type *v =				\
		    state_l(ctx, va, sizeof *v);			\
									\
		if (v == NULL)						\
			return;					\
									\
		CHECK_OBJ(v, VMODPFX_ ## TYPE ## _MAGIC);		\
									\
		if (v->protected) {					\
			VRT_fail(ctx,					\
			    "attempt to set protected variable");	\
			return;						\
		}							\
									\
		if (v == va)						\
			CP_ ## TYPE (va->var, val);			\
		else							\
			v->var = val;					\
									\
		v->defined = 1;					\
	}								\
									\
	VCL_VOID							\
	vmod_ ## type ## _undefine(VRT_CTX, struct vmodpfx_ ## type *v) \
	{								\
		v = state_l(ctx, v, sizeof *v);				\
									\
		if (v == NULL)						\
			return;					\
									\
		CHECK_OBJ(v, VMODPFX_ ## TYPE ## _MAGIC);		\
									\
		v->defined = 0;					\
	}								\
									\
	VCL_BOOL							\
	vmod_ ## type ## _defined(VRT_CTX, struct vmodpfx_ ## type *va) \
	{								\
		const struct vmodpfx_ ## type *v = state_r(ctx, va);	\
									\
		if (v == NULL)						\
			return (0);					\
									\
		CHECK_OBJ(v, VMODPFX_ ## TYPE ## _MAGIC);		\
									\
		return (!!v->defined);					\
	}								\
	VCL_VOID							\
	vmod_ ## type ## _protect(VRT_CTX, struct vmodpfx_ ## type *v)	\
	{								\
		v = state_l(ctx, v, sizeof *v);				\
									\
		if (v == NULL)						\
			return;					\
									\
		CHECK_OBJ(v, VMODPFX_ ## TYPE ## _MAGIC);		\
									\
		v->protected = 1;					\
	}								\
									\
	VCL_BOOL							\
	vmod_ ## type ## _protected(VRT_CTX,				\
	    struct vmodpfx_ ## type *va)				\
	{								\
		const struct vmodpfx_ ## type *v = state_r(ctx, va);	\
									\
		if (v == NULL)						\
			return (0);					\
									\
		CHECK_OBJ(v, VMODPFX_ ## TYPE ## _MAGIC);		\
									\
		return (!!v->protected);				\
	}
